public class Top {
	int t = 1;
	Middle mid;
	Middle.Bottom midbot;

	int readBottom() {
		mid = new Middle();
		midbot = mid.new Bottom() ;
		return midbot.b;
	}
	class Middle {
		int m = 2;
		int addTopAndBottom() {
			Bottom bottom = new Bottom();
			return Top.this.t + bottom.b;
		}
		class Bottom {
			int b = 3;
			int multiplyAllThree() {
				return this.b*Top.this.t*Middle.this.m;
			}
		}
	}
	public static void main(String[] args){
		Top top = new Top();
		System.out.println(top.readBottom());
		System.out.println(top.mid.addTopAndBottom());
		System.out.println(top.midbot.multiplyAllThree());

	}

}